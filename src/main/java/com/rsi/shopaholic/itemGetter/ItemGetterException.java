package com.rsi.shopaholic.itemGetter;

public class ItemGetterException extends Exception {

    public ItemGetterException() {}

    public ItemGetterException(Exception ex) {
        super(ex);
    }

    public ItemGetterException(String msg) {
        super(msg);
    }

}
