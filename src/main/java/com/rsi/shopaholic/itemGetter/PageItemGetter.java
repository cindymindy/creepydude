package com.rsi.shopaholic.itemGetter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.Map;

public class PageItemGetter implements ItemGetter<Document, String> {

    @Override
    public Document fetch(String url) throws ItemGetterException {
        if (url == null) {
            throw new ItemGetterException("Url cannot be null");
        }

        try {
            return Jsoup.connect(url).get();
        } catch (Exception e) {
            throw new ItemGetterException(e);
        }
    }

    public Document fetchWithCookies(String url, Map<String, String> cookies) throws ItemGetterException {
        try {
            Document doc = Jsoup.connect(url)
                    .cookies(cookies)
                    .get();
            return doc;
        } catch (Exception e) {
            throw new ItemGetterException(e);
        }
    }

}
