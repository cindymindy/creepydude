package com.rsi.shopaholic.itemGetter;

public interface ItemGetter<T, V> {

    T fetch(final V location) throws ItemGetterException;

}
