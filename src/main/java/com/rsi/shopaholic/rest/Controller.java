package com.rsi.shopaholic.rest;

import com.rsi.shopaholic.StatusDataKeeper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    private StatusDataKeeper dataKeeper = StatusDataKeeper.getINSTANCE();

    @GetMapping("/status")
    public Object status() {
        return dataKeeper;
    }

}
