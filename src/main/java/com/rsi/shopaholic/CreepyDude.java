package com.rsi.shopaholic;

import com.rsi.shopaholic.itemGetter.ItemGetterException;
import com.rsi.shopaholic.itemGetter.PageItemGetter;
import com.rsi.shopaholic.notifier.Notifier;
import com.rsi.shopaholic.notifier.NotifierException;
import com.rsi.shopaholic.watcher.Watcher;
import com.rsi.shopaholic.watcher.ps5.PS5_AmazonWatcher;
import com.rsi.shopaholic.watcher.ps5.PS5_BestBuyWatcher;
import com.rsi.shopaholic.watcher.ps5.PS5_EBGamesWatcher;
import com.rsi.shopaholic.watcher.ps5.PS5_WalmartWatcher;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CreepyDude {

    private static final long TIME_INTERVAL_SEC = 60;

    private List<Watcher> watcherList;
    private Notifier notifier;

    public CreepyDude(Notifier notifier) {
        this.notifier = notifier;
        initWatcherList();
    }

    private void initWatcherList() {
        watcherList = new ArrayList<>();
        PageItemGetter pageGetter = new PageItemGetter();
        watcherList.add(new PS5_BestBuyWatcher(pageGetter));
        watcherList.add(new PS5_EBGamesWatcher(pageGetter));
        watcherList.add(new PS5_AmazonWatcher(pageGetter));
        watcherList.add(new PS5_WalmartWatcher(pageGetter));

        // register to StatusData
        StatusDataKeeper datStatusDataKeeper = StatusDataKeeper.getINSTANCE();
        for (Watcher watcher : watcherList) {
            datStatusDataKeeper.registerStatus(watcher.getClass().getSimpleName(), watcher.getStatus());
        }
    }

    public void startCreepingNonStop() throws NotifierException {
        while (true) {
            for (Watcher watcher : watcherList) {
                final String watcherName = watcher.getClass().getSimpleName();
                try {
                    if (watcher.isAvailable()) {
                        notifier.send(watcherName + " is available!", watcher.getLocation().toString());
                    }
                } catch (ItemGetterException ex) {
                    notifier.send(watcherName + " is weird!", ex.getMessage());
                }
            }
            sleep();
        }
    }

    private void sleep() {
        try {
            TimeUnit.SECONDS.sleep(TIME_INTERVAL_SEC);
        } catch (InterruptedException e) {
            // just ignore
        }
    }

}
