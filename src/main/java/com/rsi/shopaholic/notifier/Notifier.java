package com.rsi.shopaholic.notifier;

public interface Notifier {
    void send(String subject, String htmlContent) throws NotifierException;
}
