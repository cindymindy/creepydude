package com.rsi.shopaholic.notifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

public class EmailNotifier implements Notifier {

    private static final Logger LOG = LoggerFactory.getLogger(EmailNotifier.class);

    private static final String FROM = "joseph.ryu1@gmail.com";
    private static final String TO = FROM;

    private JavaMailSender emailSender;

    public EmailNotifier(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Override
    public void send(String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(FROM);
        message.setTo(TO);
        message.setSubject(subject);
        message.setText(content);
        emailSender.send(message);
        LOG.info("Sent email: " + subject);
    }

}
