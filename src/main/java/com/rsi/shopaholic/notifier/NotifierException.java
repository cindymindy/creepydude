package com.rsi.shopaholic.notifier;

public class NotifierException extends Exception {

    public NotifierException(String msg) {
        super(msg);
    }

}
