package com.rsi.shopaholic;

import com.rsi.shopaholic.watcher.WatcherStatus;

import java.util.HashMap;
import java.util.Map;

public class StatusDataKeeper {

    private static StatusDataKeeper INSTANCE;

    private Map<String, WatcherStatus> statusList;

    private StatusDataKeeper() {
        statusList = new HashMap<>();
    }

    public static StatusDataKeeper getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new StatusDataKeeper();
        }
        return INSTANCE;
    }

    public void registerStatus(String id, WatcherStatus status) {
        statusList.put(id, status);
    }

    public Map<String, WatcherStatus> getStatusList() {
        return statusList;
    }

}
