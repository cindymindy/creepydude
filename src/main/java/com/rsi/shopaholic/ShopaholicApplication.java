package com.rsi.shopaholic;

import com.rsi.shopaholic.config.Config;
import com.rsi.shopaholic.notifier.NotifierException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ShopaholicApplication {

	private static final Logger LOG = LoggerFactory.getLogger(ShopaholicApplication.class);

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(ShopaholicApplication.class, args);
		Config config = (Config) ctx.getBean("config");
		CreepyDude creepyDude = new CreepyDude(config.getNotifier());
		try {
			creepyDude.startCreepingNonStop();
		} catch (NotifierException e) {
			LOG.error(e.getMessage());
		}
	}

}
