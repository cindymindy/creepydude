package com.rsi.shopaholic.config;

import com.rsi.shopaholic.notifier.EmailNotifier;
import com.rsi.shopaholic.notifier.Notifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;

@Configuration
public class Config {

    @Autowired
    private JavaMailSender emailSender;

    @Bean
    public Notifier getNotifier() {
        return new EmailNotifier(emailSender);
    }

}
