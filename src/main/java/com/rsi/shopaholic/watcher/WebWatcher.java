package com.rsi.shopaholic.watcher;

import com.rsi.shopaholic.itemGetter.ItemGetterException;
import com.rsi.shopaholic.itemGetter.PageItemGetter;
import org.jsoup.nodes.Document;

public abstract class WebWatcher extends Watcher<Document, String> {

    private String url;

    protected abstract boolean checkIsAvailable(final Document document) throws ItemGetterException;

    public WebWatcher(final String url, final PageItemGetter pageGetter) {
        super(pageGetter);
        this.url = url;
    }

    @Override
    public String getLocation() {
        return url;
    }

}
