package com.rsi.shopaholic.watcher.ps5;

import com.rsi.shopaholic.itemGetter.ItemGetterException;
import com.rsi.shopaholic.itemGetter.PageItemGetter;
import com.rsi.shopaholic.watcher.WebWatcher;
import org.jsoup.nodes.Document;

import java.util.HashMap;
import java.util.Map;

public class PS5_WalmartWatcher extends WebWatcher {

    private static final String URL = "https://www.walmart.ca/en/video-games/playstation-5/ps5-consoles/N-9857";

    public PS5_WalmartWatcher(final PageItemGetter pageGetter) {
        super(URL, pageGetter);
    }
    
    @Override
    protected boolean checkIsAvailable(Document document) throws ItemGetterException {
        String pageStr = document.toString();
        if (!pageStr.contains("PS5")) {
            throw new ItemGetterException("Session maybe is expired :((((");
        }
        return !document.toString().contains("https://www.walmart.ca/en/ps5-xbox-xs-out-of-stock");
    }

    @Override
    protected Document fetch() throws ItemGetterException {
        // got from postman. will they continue to be active?
        Map<String, String> loginCookies = new HashMap<>();
        loginCookies.put("userSegment", "40-percent");
        loginCookies.put("localStoreInfo", "eyJwb3N0YWxDb2RlIjoiTDVWMk42IiwibG9jYWxTdG9yZUlkIjoiMTA2MSIsInNlbGVjdGVkU3RvcmVJZCI6IjEwNjEiLCJzZWxlY3RlZFN0b3JlTmFtZSI6IkhlYXJ0bGFuZCBTdXBlcmNlbnRyZSIsImZ1bGZpbGxtZW50U3RvcmVJZCI6IjEwNjEiLCJmdWxmaWxsbWVudFR5cGUiOiJJTlNUT1JFX1BJQ0tVUCIsImFzc2lnbmVkRmFsbGJhY2siOnRydWV9");
        loginCookies.put("deliveryCatchment", "1061");
        loginCookies.put("walmart.nearestPostalCode", "L5V2N6");
        loginCookies.put("walmart.shippingPostalCode", "L5V2N6");
        loginCookies.put("defaultNearestStoreId", "1061");
        loginCookies.put("wmt.breakpoint", "1061");
        loginCookies.put("walmart.csrf", "cf9ac49bc48cb3443beaa3b6");
        loginCookies.put("wmt.c", "0");
        loginCookies.put("vtc", "W9Q6TfNPW-UVr0IFEVWR8g; TS01f4281b=01c5a4e2f927a2826dd8cf0cef08b4d64b5b0038c54e86634f72d3ead64b7d935b2028fa1aabc68ba5e06bb190d21ad8af72cc664e");
        return ((PageItemGetter) getItemGetter()).fetchWithCookies(URL, loginCookies);
    }

}
