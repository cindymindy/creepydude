package com.rsi.shopaholic.watcher.ps5;

import com.rsi.shopaholic.itemGetter.PageItemGetter;
import com.rsi.shopaholic.itemGetter.ItemGetterException;
import com.rsi.shopaholic.watcher.WebWatcher;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class PS5_BestBuyWatcher extends WebWatcher {

    private static final String URL = "https://www.bestbuy.ca/en-ca/product/playstation-5-console-online-only/14962185";
    private static final String ADD_TO_CART_BUTTON_CLASS = "addToCartButton";

    public PS5_BestBuyWatcher(final PageItemGetter pageGetter) {
        super(URL, pageGetter);
    }

    @Override
    protected boolean checkIsAvailable(Document document) throws ItemGetterException {
        Elements elements = document.getElementsByClass(ADD_TO_CART_BUTTON_CLASS);
        if (elements.size() == 0) {
            throw new ItemGetterException("No add to cart button found");
        }
        if (elements.size() > 1) {
            throw new ItemGetterException("Multiple add to cart buttons found");
        }
        return !elements.hasAttr("disabled");
    }

}
