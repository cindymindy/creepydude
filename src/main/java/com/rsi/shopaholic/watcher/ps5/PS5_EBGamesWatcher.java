package com.rsi.shopaholic.watcher.ps5;

import com.rsi.shopaholic.itemGetter.PageItemGetter;
import com.rsi.shopaholic.itemGetter.ItemGetterException;
import com.rsi.shopaholic.watcher.WebWatcher;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class PS5_EBGamesWatcher extends WebWatcher {

    private static final String URL = "https://www.ebgames.ca/PS5/Games/877522/playstation-5";
    private static final String ADD_TO_CART_BUTTON_ID = "btnAddToCart";

    public PS5_EBGamesWatcher(final PageItemGetter pageGetter) {
        super(URL, pageGetter);
    }
    
    @Override
    protected boolean checkIsAvailable(Document document) throws ItemGetterException {
        Element addToCartButton = document.getElementById(ADD_TO_CART_BUTTON_ID);
        if (addToCartButton == null) {
            throw new ItemGetterException("No add to cart button found");
        }
        return !addToCartButton.attr("style").contains("display:none");
    }

}
