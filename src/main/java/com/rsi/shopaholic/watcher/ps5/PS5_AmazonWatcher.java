package com.rsi.shopaholic.watcher.ps5;

import com.rsi.shopaholic.itemGetter.ItemGetterException;
import com.rsi.shopaholic.itemGetter.PageItemGetter;
import com.rsi.shopaholic.watcher.WebWatcher;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.HashMap;
import java.util.Map;

public class PS5_AmazonWatcher extends WebWatcher {

    private static final String URL = "https://www.amazon.ca/PlayStation-5-Console/dp/B08GSC5D9G";
    private static final String ADD_TO_CART_BUTTON_ID = "add-to-cart-button";

    public PS5_AmazonWatcher(final PageItemGetter pageGetter) {
        super(URL, pageGetter);
    }
    
    @Override
    protected boolean checkIsAvailable(Document document) throws ItemGetterException {
        if (!document.toString().contains("PlayStation")) {
            throw new ItemGetterException("Session maybe is expired :((((");
        }
        Element addToCartButton = document.getElementById(ADD_TO_CART_BUTTON_ID);
        if (addToCartButton == null) {
            return false;
        }
        return true;
    }

    @Override
    protected Document fetch() throws ItemGetterException {
        // got from postman. will they continue to be active?
        Map<String, String> loginCookies = new HashMap<>();
        loginCookies.put("session-id", "140-7158737-4761461");
        loginCookies.put("session-id-time", "2082787201l");
        loginCookies.put("i18n-prefs", "CAD");
        loginCookies.put("ubid-acbca", "132-0337255-8421939");
        loginCookies.put("session-token", "h55PpgUQa5OBctYKYAccxuQ3oQxsvtg3lvLNQnyGGfVvsD7mgKJ+4QyXRISWbqWC+pQaMIHGeezT47Hs1dJdB/b9bp+QUHAaY6DNKkliXUw1yc3LLk74RAA6/zZe+viqoDYoRh/CI1xfBjW15M0uggzFelflWvk2y9lU4LalK/12p2eGmW5viyfWLe6ZL8nE");
        return ((PageItemGetter) getItemGetter()).fetchWithCookies(URL, loginCookies);
    }

}
