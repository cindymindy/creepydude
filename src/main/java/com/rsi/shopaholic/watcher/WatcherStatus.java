package com.rsi.shopaholic.watcher;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class WatcherStatus {

    private LocalDateTime lastChecked;
    private List<LocalDateTime> availTimeStampList;

    public WatcherStatus() {
        this.availTimeStampList = new ArrayList<>();
    }

    public LocalDateTime getLastChecked() {
        return lastChecked;
    }

    public void setLastChecked(LocalDateTime lastChecked) {
        this.lastChecked = lastChecked;
    }

    public List<LocalDateTime> getAvailTimeStampList() {
        return availTimeStampList;
    }

    public void setAvailTimeStampList(List<LocalDateTime> availTimeStampList) {
        this.availTimeStampList = availTimeStampList;
    }

}
