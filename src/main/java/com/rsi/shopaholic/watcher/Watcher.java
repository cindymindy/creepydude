package com.rsi.shopaholic.watcher;

import com.rsi.shopaholic.itemGetter.ItemGetter;
import com.rsi.shopaholic.itemGetter.ItemGetterException;

import java.time.LocalDateTime;

/*
    T and V should match whatever ItemGetter it has
 */
public abstract class Watcher<T, V> {

    private WatcherStatus status;
    private ItemGetter itemGetter;

    public Watcher(ItemGetter itemGetter) {
        this.status = new WatcherStatus();
        this.itemGetter = itemGetter;
    }

    public abstract V getLocation();

    protected abstract boolean checkIsAvailable(final T document) throws ItemGetterException;

    public Boolean isAvailable() throws ItemGetterException {
        T itemInfo = fetch();
        if (itemInfo == null) {
            throw new ItemGetterException("Document is null");
        }
        boolean isAvailable = checkIsAvailable(itemInfo);
        updateStatus(isAvailable);
        return isAvailable;
    }

    protected T fetch() throws ItemGetterException {
        return getItemGetter().fetch(getLocation());
    }

    protected void updateStatus(boolean isAvailable) {
        LocalDateTime now = LocalDateTime.now();
        status.setLastChecked(now);
        if (isAvailable) {
            status.getAvailTimeStampList().add(now);
        }
    }

    public WatcherStatus getStatus() {
        return status;
    }

    public ItemGetter<T, V> getItemGetter() {
        return itemGetter;
    }

}
