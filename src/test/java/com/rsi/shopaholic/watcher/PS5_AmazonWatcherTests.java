package com.rsi.shopaholic.watcher;

import com.rsi.shopaholic.itemGetter.ItemGetterException;
import com.rsi.shopaholic.itemGetter.PageItemGetter;
import com.rsi.shopaholic.watcher.ps5.PS5_AmazonWatcher;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class PS5_AmazonWatcherTests {

	@Test
	void isAvailableReturnsValue() throws ItemGetterException {
		PageItemGetter pageGetter = new PageItemGetter();
		PS5_AmazonWatcher watcher = new PS5_AmazonWatcher(pageGetter);
		Boolean available = watcher.isAvailable();
		assertNotNull(available);
	}

}
