package com.rsi.shopaholic.watcher;

import com.rsi.shopaholic.itemGetter.ItemGetterException;
import com.rsi.shopaholic.itemGetter.PageItemGetter;
import com.rsi.shopaholic.watcher.ps5.PS5_WalmartWatcher;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class PS5_WalmartWatcherTests {

	@Test
	void isAvailableReturnsValue() throws ItemGetterException {
		PageItemGetter pageGetter = new PageItemGetter();
		Watcher watcher = new PS5_WalmartWatcher(pageGetter);
		Boolean available = watcher.isAvailable();
		assertNotNull(available);
	}

}
