package com.rsi.shopaholic.itemGetter;

import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ItemGetterTests {

	@Test
	void pageGetterFetchesThrowsExceptionIfUrlNull() {
		PageItemGetter pageGetter = new PageItemGetter();
		assertThrows(ItemGetterException.class, () -> pageGetter.fetch(null));
	}

	@Test
	void pageGetterFetchesDocument() throws ItemGetterException {
		PageItemGetter pageGetter = new PageItemGetter();
		Document document = pageGetter.fetch("https://www.google.com");
		assertNotNull(document);
	}

	@Test
	void pageGetterFetchesThrowsExceptionIfUrlNotExist() {
		PageItemGetter pageGetter = new PageItemGetter();
		assertThrows(ItemGetterException.class, () -> pageGetter.fetch("https://www.thispagedoesnotexistJRJRJR.com"));
	}

}
